<img src="images/readme/header-small.jpg" >

# E. Navigation avancée  <!-- omit in toc -->

_**Pour terminer ce TP, nous allons finaliser le mécanisme de navigation dans la page :**_
- les liens du menu doivent indiquer sur quelle page on se trouve
- on doit pouvoir utiliser les boutons précédent/suivant du navigateur
- lorsqu'on recharge la page dans le navigateur (F5) on doit afficher la page sur laquelle l'utilisateur se trouvait

## Sommaire <!-- omit in toc -->
- [E.1. Mise à jour de la liste](#e1-mise-à-jour-de-la-liste)
- [D.5. Le formulaire complet :](#d5-le-formulaire-complet-)
- [E.2. Formulaire d'ingrédients](#e2-formulaire-dingrédients)

## E.1. activation du menu
**Actuellement lorsqu'on clique sur un lien du menu, rien n'indique dans le menu sur quelle page on se trouve.** On va donc remédier à ça en ajoutant une **classe CSS** sur le lien qui correspond à la page sur laquelle on se rend (_notez le trait plus grand sur le lien cliqué_) :

<img src="./images/readme/nav-active.gif">

**Dans la méthode `Router.navigate()` :**
1. **ajoutez la classe CSS "active"** sur la balise `<a>` qui correspond au `path` passé à `Router.navigate()`

	> _**NB :** Pour simplifier le travail, plutôt que d'utiliser l'instruction `setAttribute('class', ...)` **je vous recommande plutôt la propriété [`element.classList` (_mdn)_](https://developer.mozilla.org/fr/docs/Web/API/Element/classList) et ses méthodes `element.classList.add()` et `element.classList.remove()`** qui permettent de ne pas se soucier des autres classes CSS déjà présentes sur les balises en plus de la classe `"active"`_

2. **enlevez la classe `"active"` sur le précédent lien actif** (de manière à n'avoir qu'un seul lien actif à la fois)

## E.2. History API

_**Notre SPA commence à ressembler à quelque chose mais souffre encore d'un GROS PROBLÈME par rapport à une application web "classique" (celles avec rechargement de page) : impossible d'utiliser les boutons précédent/suivant du navigateur !**_

Heureusement il existe une API JS qui va nous permettre de résoudre ce problème : **la [History API (mdn)](https://developer.mozilla.org/fr/docs/Web/Guide/DOM/Manipuler_historique_du_navigateur)** .\
Cette API est un ensemble de méthodes de l'objet `window`, disponibles de base, et qui permettent de **manipuler l'URL dans la barre d'adresse du navigateur SANS rechargement de page** !

Quand l'utilisateur change de page (_pour passer de la liste au formulaire par exemple_) on va pouvoir modifier, en JS, l'url courante :

<img src="images/readme/changement-url.gif" />

Ce qui permet ensuite d'utiliser les boutons précédent/suivant du navigateur (_là aussi sans rechargement de page_) :

<img src="images/readme/boutons-prev-next.gif" />

1. **Dans la méthode `Router.navigate()`, appelez [`window.history.pushState()` _(mdn)_](https://developer.mozilla.org/en-US/docs/Web/API/History/pushState) pour modifier l'URL de la page.**

	> _**NB :** Si vous lisez la documentation en détail vous verrez qu'au final peu importe la valeur des 2 premiers paramètres de `pushState()` (ils peuvent être `null`), c'est surtout le 3e qui est important._

2. **L'URL change maintenant à chaque fois que vous cliquez sur un lien du menu, mais si vous utilisez les boutons précédent/suivant du navigateur, vous verrez que rien ne se passe.** Il reste en effet à détecter le changement d'URL dans notre code pour **invoquer `Router.navigate()` à chaque fois que l'utilisateur utilise les boutons précédent/suivant**.

	Pour celà nous avons à notre disposition 2 outils :
	- le **callback [`window.onpopstate` _(mdn)_](https://developer.mozilla.org/fr/docs/Web/API/WindowEventHandlers/onpopstate)** qui permet d'appeler une fonction lors de l'utilisation des boutons précédent/suivant du navigateur
	- et la **propriété [`document.location` _(mdn)_](https://developer.mozilla.org/fr/docs/Web/API/Document/location)** qui permet d'obtenir des informations sur l'URL de la page courante

	Dans `src/main.js`, assignez une fonction (un arrow function par exemple) à `window.onpopstate` et placez-y pour le moment juste un `console.log(document.location)`.

	Rechargez la page, naviguez un peu via le menu et les boutons précédent/suivant et inspectez dans la console le contenu de `document.location`, vous devriez y trouver quelque chose qui peut vous aider à afficher la bonne page...

	> _**NB :** faites attention à ne pas invoquer `window.history.pushState()` lors du `onpopstate` sinon cela va quelque peu "casser" la navigation avec les boutons précédent/suivant en créant une "inception"..._


## E.3. Le Deeplinking

_**Notre application est presque terminée, à un détail près : l'absence de [deep linking](https://fr.wikipedia.org/wiki/Lien_profond).**_

En effet, si vous vous rendez directement sur http://localhost:8000/a-propos dans votre navigateur (_sans passer par la racine_), le serveur vous retourne une erreur 404 à la place de la page de détail.

<img src="images/readme/404.gif" />

En fait, lorsque vous lancez la requête en entrant cette URL dans la barre d'adresse du navigateur, le serveur http lancé avec `npx serve` cherche par défaut à trouver un fichier `a-propos.html` ou bien un fichier `index.html` dans un sous dossier `/a-propos/`. Autant dire que ça n'a aucune chance de fonctionner comme ça.

Heureusement `npx serve` dispose d'une option `-s` qui permet de rediriger toutes les 404 vers le `index.html` de la racine : ainsi notre JS se chargera et il ne restera plus qu'à déterminer (en JS) quelle page afficher grâce à l'URL courante.

1. **Stoppez le serveur HTTP `npx serve -l 8000` et relancez le avec cette fois l'option `-s` :**
	```bash
	npx serve -s -l 8000
	```
	Rechargez la page http://localhost:8000/a-propos : la 404 a disparu est le site s'affiche ! Malheureusement c'est encore la `PizzaList` qui est affichée et pas la page "À propos"...

2. **Maintenant que notre site s'affiche quelque soit l'URL de la page, reste à faire en sorte que l'on affiche la page qui correspond à l'URL demandée par l'utilisateur.**

	Actuellement, au chargement, notre application affiche toujours la `PizzaList` en premier car dans notre `main.js` nous avons le code :
	```js
	Router.navigate('/');
	```
	La solution est simple : il suffit de remplacer cette URL en dur (`'/'`) par l'URL de la page courante.

	Ça tombe bien, dans l'exercice précédent vous avez découvert la propriété qui permet de récupérer l'URL courante de la barre d'adresse. Ne reste plus qu'à envoyer cette valeur au `Router.navigate()` initial et le tour est joué !

_**À partir de maintenant, vous pouvez en principe charger le site depuis n'importe quelle URL ! Youpi. Merci le deep linking !**_

<img src="images/readme/deeplinking.gif" />
