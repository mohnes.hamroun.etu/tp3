export default class Router {
	static titleElement;
	static contentElement;
	static routes = [];

	static navigate(path) {
		const route = this.routes.find(route => route.path === path);
		if (route) {
			this.titleElement.innerHTML = `<h1>${route.title}</h1>`;
			this.contentElement.innerHTML = route.page.render();
		}
	}
}
